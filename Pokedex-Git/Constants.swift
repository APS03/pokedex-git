//
//  Constants.swift
//  Pokedex-Git
//
//  Created by Amin Piña on 9/29/16.
//  Copyright © 2016 Amin Piña. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

public typealias DownloadComplete = () -> ()