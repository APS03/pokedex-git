//
//  PokeCell.swift
//  Pokedex-Git
//
//  Created by Amin Piña on 9/27/16.
//  Copyright © 2016 Amin Piña. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var numLbl: UILabel!
    
    var pokemon: Pokemon!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5.0
        
    }
    
    func configureCell(pokemon: Pokemon){
        self.pokemon = pokemon
        
        nameLbl.text = self.pokemon.name.capitalizedString
        numLbl.text = ("\(self.pokemon.pokedexId)")
        thumbImg.image = UIImage(named: "\(self.pokemon.pokedexId)")
    }
    
}
